package sample.contacts.ui.detail;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import sample.contacts.R;
import sample.contacts.commons.Constants;
import sample.contacts.domain.Contact;
import sample.contacts.domain.Location;
import sample.contacts.ui.DetailCardView;
import sample.contacts.utils.StringHelper;

public class DetailFragment extends Fragment {

    public interface IDetailCallback {
        void onPopulateViews(String title, String imageUri);
    }

    private IDetailCallback iDetailCallback;
    private Contact contact;
    private DetailCardView contactDetailCard;
    private DetailCardView addressDetailCard;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            iDetailCallback = (IDetailCallback) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement IDetailCallback");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.detail_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        contactDetailCard = view.findViewById(R.id.contactData);
        addressDetailCard = view.findViewById(R.id.addressData);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            contact = (Contact) savedInstanceState.get(Constants.CONTACT);
        } else {
            contact = (Contact) getArguments().getSerializable(Constants.CONTACT);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        populateViews();
    }

    private void populateViews() {
        populateContactDetail();
        populateAddressDetail();
        populateToolbarData();
    }

    private void populateContactDetail() {
        contactDetailCard.setFieldData(StringHelper.getStringFormatted(
                contact.getEmail(), R.string.email_field, getActivity()),
                null, null);
    }

    private void populateAddressDetail() {
        Location location = contact.getLocation();
        if (location != null) {
            addressDetailCard.setFieldData(StringHelper.getStringFormatted(location.getCity(), R.string.city_field, getActivity()),
                    StringHelper.getStringFormatted(location.getStreet(), R.string.street_field, getActivity()),
                    StringHelper.getStringFormatted(location.getState(), R.string.state_field, getActivity()));
        }
    }

    private void populateToolbarData() {
        String fullName = StringHelper.capitalize(contact.getName().getLast())
                + " " + StringHelper.capitalize(contact.getName().getFirst());
        iDetailCallback.onPopulateViews(fullName, contact.getPicture().getLarge());
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(Constants.CONTACT, contact);
    }
}