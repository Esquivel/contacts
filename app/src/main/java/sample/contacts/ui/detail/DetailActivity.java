package sample.contacts.ui.detail;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import sample.contacts.R;

public class DetailActivity extends AppCompatActivity implements DetailFragment.IDetailCallback {

    private ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.default_activity_toolbar_collapsible_layout);

        if (savedInstanceState == null) {
            Bundle bundle = getIntent().getExtras();
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            DetailFragment detailFragment = new DetailFragment();
            detailFragment.setArguments(bundle);
            fragmentTransaction.add(R.id.mainContainer, detailFragment);
            fragmentTransaction.commit();
        }

        image = findViewById(R.id.image);
        setToolbar();
    }

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbarCollapsible);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onPopulateViews(String title, String imageUri) {
        setName(title);
        setImage(imageUri);
    }

    private void setName(@NonNull String titleName) {
        getSupportActionBar().setTitle(titleName);
    }

    private void setImage(String imageUri) {
        Picasso.with(this)
                .load(imageUri)
                .fit()
                .centerCrop()
                .placeholder(R.drawable.ic_launcher_background)
                .into(image);
    }
}
