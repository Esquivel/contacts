package sample.contacts.ui.master;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import sample.contacts.R;
import sample.contacts.domain.Contact;
import sample.contacts.repository.ContactRepository;

public class MasterFragment extends Fragment implements ContactRepository.CallbackContactRepository {

    public interface IClickAction {
        void onContactClick(Contact contact, View view);
    }

    private static final String CONTACTS = "contacts";

    private IClickAction iClickAction;
    private List<Contact> contactList = new ArrayList<>();
    private MasterAdapter masterAdapter;
    private ContactRepository contactRepository;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            iClickAction = (IClickAction)context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement iClickAction");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        contactRepository = new ContactRepository(getActivity(), this);
        masterAdapter = new MasterAdapter(new MasterAdapter.IAdapterCallback() {
            @Override
            public void onClick(Contact contact, View view) {
                iClickAction.onContactClick(contact, view);
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.master_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView recyclerView = view.findViewById(R.id.recycler);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(masterAdapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            contactList = (List<Contact>) savedInstanceState.get(CONTACTS);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.master_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh_data:
                contactRepository.refreshCache();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (contactList.isEmpty()) {
            contactRepository.getContacts();
        } else {
            populateViews();
        }

    }

    @Override
    public void onGetContacts(List<Contact> contacts) {
        contactList = contacts;
        populateViews();
    }

    private void populateViews() {
        masterAdapter.addData(contactList);
    }

    @Override
    public void onPause() {
        super.onPause();
        contactRepository.unSubscribe();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(CONTACTS, (Serializable) contactList);
    }
}