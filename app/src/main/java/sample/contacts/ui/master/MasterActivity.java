package sample.contacts.ui.master;

import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import sample.contacts.R;
import sample.contacts.commons.Constants;
import sample.contacts.domain.Contact;
import sample.contacts.ui.detail.DetailActivity;

public class MasterActivity extends AppCompatActivity implements MasterFragment.IClickAction {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.default_activity_layout);

        if (savedInstanceState == null) {
            Bundle bundle = getIntent().getExtras();
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            MasterFragment masterFragment = new MasterFragment();
            masterFragment.setArguments(bundle);
            fragmentTransaction.add(R.id.mainContainer, masterFragment);
            fragmentTransaction.commit();
        }

        setToolbar();
    }

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.title_master_activity));
    }

    @Override
    public void onContactClick(Contact contact, View view) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra(Constants.CONTACT, contact);
        startActivity(intent, createAnimation(view));
    }

    private Bundle createAnimation(View view) {
        Pair<View, String> p1 = Pair.create(view, Constants.VIEW_TRANSITION_CODE);
        ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation(this, p1);
        return options.toBundle();
    }
}