package sample.contacts.ui.master;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import sample.contacts.R;
import sample.contacts.domain.Contact;
import sample.contacts.utils.StringHelper;

class MasterAdapter extends RecyclerView.Adapter<MasterAdapter.ContactItem> {

    public interface IAdapterCallback {
        void onClick(Contact contact, View view);
    }

    private List<Contact> contacts = new ArrayList<>();
    private IAdapterCallback iAdapterCallback;

    MasterAdapter(IAdapterCallback iAdapterCallback) {
        this.iAdapterCallback = iAdapterCallback;
    }

    public ContactItem onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_adapter, parent, false);
        return new ContactItem(view);
    }

    @Override
    public void onBindViewHolder(ContactItem holder, int position) {
        Contact contact = this.contacts.get(position);
        Context context = holder.first.getContext();
        holder.first.setText(context.getString(R.string.first_name_field, StringHelper.capitalize(contact.getName().getFirst())));
        holder.last.setText(context.getString(R.string.last_name_field, StringHelper.capitalize(contact.getName().getLast())));

        Picasso.with(holder.image.getContext())
                .load(contact.getPicture().getThumbnail())
                .fit()
                .centerCrop()
                .placeholder(R.drawable.ic_launcher_background)
                .into(holder.image);
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    void addData(List<Contact> contacts) {
        this.contacts = new ArrayList<>(contacts);
        notifyDataSetChanged();
    }

    class ContactItem extends RecyclerView.ViewHolder {

        private ImageView image;
        private TextView first;
        private TextView last;

        ContactItem(final View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            first = itemView.findViewById(R.id.first);
            last = itemView.findViewById(R.id.last);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iAdapterCallback.onClick(contacts.get(getAdapterPosition()), itemView);
                }
            });
        }
    }
}