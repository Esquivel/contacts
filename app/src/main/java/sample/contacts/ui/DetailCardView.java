package sample.contacts.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import sample.contacts.R;

public class DetailCardView extends LinearLayout {

    private TextView firstTextView;
    private TextView secondTextView;
    private TextView thirdTextView;

    public DetailCardView(Context context) {
        super(context);
        init(context, null);
    }

    public DetailCardView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public DetailCardView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        setOrientation(VERTICAL);
        LayoutInflater.from(context).inflate(R.layout.detail_card_view, this);

        TextView titleCard = findViewById(R.id.titleCard);
        firstTextView = findViewById(R.id.firstTextView);
        secondTextView = findViewById(R.id.secondTextView);
        thirdTextView = findViewById(R.id.thirdTextView);

        this.setVisibility(GONE);

        if(attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.DetailCardView);
            if(a.hasValue(R.styleable.DetailCardView_cardTitle)) {
                titleCard.setText(a.getString(R.styleable.DetailCardView_cardTitle));
            }
            a.recycle();
        }
    }

    /**
     * Use this method to set data to the view.
     * Only shows view if at least one parameter != null.
     *
     * @param firstText
     * @param secondText
     * @param thirdText
     */
    public void setFieldData(@Nullable String firstText, @Nullable String secondText,
                             @Nullable String thirdText) {
        if (firstText != null) {
            this.setVisibility(VISIBLE);
            firstTextView.setVisibility(VISIBLE);
            firstTextView.setText(firstText);
        }

        if (secondText != null) {
            this.setVisibility(VISIBLE);
            secondTextView.setVisibility(VISIBLE);
            secondTextView.setText(secondText);
        }

        if (thirdText != null) {
            this.setVisibility(VISIBLE);
            thirdTextView.setVisibility(VISIBLE);
            thirdTextView.setText(thirdText);
        }
    }
}