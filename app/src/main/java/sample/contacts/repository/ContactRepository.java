package sample.contacts.repository;

import android.content.Context;

import java.util.List;

import sample.contacts.commons.ContactCache;
import sample.contacts.domain.Contact;
import sample.contacts.service.ContactService;

public class ContactRepository implements ContactService.IContactCallback {

    public interface CallbackContactRepository {
        void onGetContacts(List<Contact> contacts);
    }

    private ContactService contactService;
    private ContactCache contactCache;
    private CallbackContactRepository callbackContactRepository;

    public ContactRepository(Context context, CallbackContactRepository callbackContactRepository) {
        this.contactService = ContactService.getInstance();
        this.contactCache = new ContactCache(context);
        this.callbackContactRepository = callbackContactRepository;
    }

    public void getContacts() {
        if (contactCache.getContacts().isEmpty()) {
            contactService.getContacts(this);
        } else {
            this.callbackContactRepository.onGetContacts(contactCache.getContacts());
        }
    }

    public void refreshCache() {
        contactCache.expireData();
        getContacts();
    }

    @Override
    public void onResponseOk(List<Contact> contacts) {
        this.contactCache.setContacts(contacts);
        this.callbackContactRepository.onGetContacts(contactCache.getContacts());
    }

    public void unSubscribe() {
        contactService.unSubscribe();
    }
}