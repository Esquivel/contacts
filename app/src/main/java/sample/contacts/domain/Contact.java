package sample.contacts.domain;

import android.support.annotation.NonNull;

import java.io.Serializable;

public class Contact implements Serializable, Comparable<Contact> {

    private Name name;
    private String email;
    private Picture picture;
    private Location location;

    public Name getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public Picture getPicture() {
        return picture;
    }

    public Location getLocation() {
        return location;
    }

    @Override
    public int compareTo(@NonNull Contact o) {
        int compareResultLast = this.getName().getLast().compareTo(o.getName().getLast());
        return compareResultLast == 0 ? this.getName().getFirst().compareTo(o.getName().getFirst()) : compareResultLast;
    }
}