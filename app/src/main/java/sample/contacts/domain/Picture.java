package sample.contacts.domain;

import java.io.Serializable;

public class Picture implements Serializable {

    private String thumbnail;
    private String medium;
    private String large;

    public String getThumbnail() {
        return thumbnail;
    }

    public String getMedium() {
        return medium;
    }

    public String getLarge() {
        return large;
    }
}
