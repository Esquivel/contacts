package sample.contacts.domain;

import java.io.Serializable;

public class Name implements Serializable {

    private String first;
    private String last;

    public String getFirst() {
        return first;
    }

    public String getLast() {
        return last;
    }
}
