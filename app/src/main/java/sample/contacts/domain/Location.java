package sample.contacts.domain;

import java.io.Serializable;

public class Location implements Serializable {

    private String street;
    private String city;
    private String state;

    public String getStreet() {
        return street;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }
}