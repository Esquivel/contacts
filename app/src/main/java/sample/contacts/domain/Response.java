package sample.contacts.domain;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response {

    @SerializedName("results")
    private List<Contact> contactList;

    public List<Contact> getContactList() {
        return contactList;
    }
}
