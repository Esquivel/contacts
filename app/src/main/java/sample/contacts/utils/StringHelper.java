package sample.contacts.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class StringHelper {

    public static @NonNull String capitalize(@NonNull String stringToCapitalize) {
        return stringToCapitalize.substring(0, 1).toUpperCase() + stringToCapitalize.substring(1);
    }

    public static @Nullable String getStringFormatted(@Nullable String stringToFormat,
                                                       int fieldNameRes, Context context) {
        return stringToFormat != null && !stringToFormat.isEmpty() ?
                context.getApplicationContext().getString(fieldNameRes, stringToFormat) : null;
    }
}
