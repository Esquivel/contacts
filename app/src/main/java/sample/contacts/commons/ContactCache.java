package sample.contacts.commons;

import android.content.Context;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.Weeks;
import java.util.ArrayList;
import java.util.List;

import sample.contacts.domain.Contact;

public class ContactCache {

    private static final int EXPIRATION_LIMIT_WEEK = 1;
    private ContactSharedPref contactSharedPref;

    public ContactCache(Context context) {
        contactSharedPref = new ContactSharedPref(context);
    }

    public List<Contact> getContacts() {
        expireDataIfNeeded();
        return contactSharedPref.getContacts();
    }

    public void setContacts(List<Contact> contacts) {
        contactSharedPref.setContacts(contacts, getExpirationStringDate());
    }

    private String getExpirationStringDate() {
        return getNow().toString();
    }

    private void expireDataIfNeeded() {
        DateTime expirationDate = new DateTime(contactSharedPref.getExpirationDate());

        int differenceInWeeks = Weeks.weeksBetween(expirationDate, new DateTime(getNow())).getWeeks();
        if (differenceInWeeks > EXPIRATION_LIMIT_WEEK) {
            expireData();
        }
    }

    public void expireData() {
        contactSharedPref.setContacts(new ArrayList<Contact>(), getExpirationStringDate());
    }

    private DateTime getNow() {
        return new DateTime(LocalDate.now().toString("yyyy-MM-dd"));
    }
}