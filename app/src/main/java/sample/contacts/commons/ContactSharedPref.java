package sample.contacts.commons;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import sample.contacts.domain.Contact;

public class ContactSharedPref extends AbstractSharedPreferences {

    private static final String CONTACT_SHARED_PREFERENCES = "contact_shared_preferences";
    private static final String CONTACTS = "contacts";
    private static final String EXPIRATION_DATE = "expiration_date";

    public ContactSharedPref(Context context) {
        init(context, CONTACT_SHARED_PREFERENCES);
    }

    public void setContacts(List<Contact> contacts, String expirationDate) {
        Gson gson = new Gson();
        String contactsToJson = gson.toJson(contacts);
        getEditor().putString(CONTACTS, contactsToJson);
        getEditor().putString(EXPIRATION_DATE, expirationDate);
        applyChanges();
    }

    public List<Contact> getContacts(){
        Gson gson = new Gson();
        Type type = new TypeToken<List<Contact>>() {}.getType();
        return new ArrayList<>((List<Contact>)gson.fromJson(getPref().getString(CONTACTS, new Gson().toJson(new ArrayList<Contact>())), type));
    }

    public String getExpirationDate() {
        return getPref().getString(EXPIRATION_DATE, null);
    }
}