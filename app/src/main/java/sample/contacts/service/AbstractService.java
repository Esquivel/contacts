package sample.contacts.service;

import java.net.HttpURLConnection;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.HttpException;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Subscription;

public abstract class AbstractService implements IServiceCallback {

    private static final int TIME_OUT = 20;
    private static final String BASE_URL = "https://randomuser.me";
    private static IEndpointManager iEndpointManager;
    private Subscription subscription;

    protected abstract void init();

    protected IEndpointManager get() {
        if (iEndpointManager == null) {
            createInstance();
        }
        return iEndpointManager;
    }

    private void createInstance() {
        iEndpointManager = new Retrofit
                .Builder()
                .baseUrl(BASE_URL)
                .client(setClientTimeOut(TIME_OUT))
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build()
                .create(IEndpointManager.class);
    }

    private OkHttpClient setClientTimeOut(int timeOut) {
        return new OkHttpClient.Builder()
                .readTimeout(timeOut, TimeUnit.SECONDS)
                .connectTimeout(timeOut, TimeUnit.SECONDS)
                .build();
    }

    protected void setSubscription(Subscription subscription) {
        this.subscription = subscription;
    }

    protected void unSubscribeService() {
        if (subscription != null) {
            subscription.unsubscribe();
        }
    }

    protected void onError(Throwable e) {
        if (e instanceof HttpException) {
            HttpException response = (HttpException)e;
            int code = response.code();
            if (code == HttpURLConnection.HTTP_NOT_FOUND) {
                this.onResponseNoData();
            } else if (code == HttpURLConnection.HTTP_CLIENT_TIMEOUT) {
                this.onResponseTimeOut();
            } else {
                this.onResponseError();
            }
        } else {
            this.onResponseConnectionError();
        }
    }

    @Override
    public void onResponseNoData() {
        //Implement this if is needed
    }

    @Override
    public void onResponseError() {
        //Implement this if is needed
    }

    @Override
    public void onResponseTimeOut() {
        //Implement this if is needed
    }

    @Override
    public void onResponseConnectionError() {
        //Implement this if is needed
    }
}