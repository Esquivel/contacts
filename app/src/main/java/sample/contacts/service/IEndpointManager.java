package sample.contacts.service;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;
import sample.contacts.domain.Response;

public interface IEndpointManager {

    @GET("api")
    Observable<Response> getRandomUsers(@Query("results") int results);

}
