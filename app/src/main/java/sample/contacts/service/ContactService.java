package sample.contacts.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import sample.contacts.domain.Contact;
import sample.contacts.domain.Response;

public class ContactService extends AbstractService {

    public interface IContactCallback {
        void onResponseOk(List<Contact> contacts);
    }

    private static final int QUANTITY_PROFILE = 50;

    private IContactCallback iCallback;
    private IEndpointManager iEndpointManager;
    private static ContactService instance;

    public static ContactService getInstance() {
        if (instance == null) {
            instance = new ContactService();
        }
        return instance;
    }

    private ContactService() {
        init();
    }

    @Override
    protected void init() {
        iEndpointManager = get();
    }

    public void getContacts(IContactCallback iRandomUserCallback) {
        this.iCallback = iRandomUserCallback;
        doRequest();
    }

    private void doRequest() {
        Observable<Response> call = iEndpointManager.getRandomUsers(QUANTITY_PROFILE);
        setSubscription(call.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(normalizeResponse())
                .subscribe(createSubscriberGetContacts()));
    }

    /**
     * This method do all operations to ensure the correct state of the response obtained.
     * Must be execute on a worker thread.
     * @return
     */
    private Func1<Response, Observable<List<Contact>>> normalizeResponse() {
        return new Func1<Response, Observable<List<Contact>>>() {
            @Override
            public Observable<List<Contact>> call(Response response) {
                List<Contact> depuratedList = new ArrayList<>();
                depurate(response.getContactList(), depuratedList);
                sort(depuratedList);
                return Observable.just(depuratedList);
            }
        };
    }

    private void depurate(List<Contact> contacts, List<Contact> depuratedList) {
        for (Contact contact : contacts) {
            if (contact.getName() != null &&
                    contact.getName().getFirst() != null &&
                    !contact.getName().getFirst().isEmpty() &&
                    contact.getName().getLast() != null &&
                    !contact.getName().getLast().isEmpty()) {
                depuratedList.add(contact);
            }
        }
    }

    private void sort(List<Contact> contacts) {
        Collections.sort(contacts);
    }

    private Subscriber<List<Contact>> createSubscriberGetContacts() {
        return new Subscriber<List<Contact>>() {
            @Override
            public void onCompleted() {
                //Nothing to do
            }

            @Override
            public void onError(Throwable e) {
                ContactService.this.onError(e);
            }

            @Override
            public void onNext(List<Contact> contacts) {
                if (contacts.size() > 0) {
                    iCallback.onResponseOk(Collections.unmodifiableList(contacts));
                } else {
                    ContactService.this.onResponseNoData();
                }
            }
        };
    }

    public void unSubscribe() {
        unSubscribeService();
    }
}